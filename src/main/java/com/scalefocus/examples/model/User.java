package com.scalefocus.examples.model;

import org.springframework.beans.factory.annotation.Value;

/**
 * @author Angel Zlatenov
 */

public class User {

    private String email;

    @Value("$$${default.username|Random Name}}") // 17. - PropertySourcesPlaceholderConfigurer
    private String name;

    public String getEmail() {
        return this.email;
    }

    @Value("$$${admin.email}}") // 17. - PropertySourcesPlaceholderConfigurer
    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void customInitMethod() { // 19. initialization method
        System.out.println("Im in customInitMethod for user Bean");
    }

    public void customDestroyMethod() { // 20. Destroy method
        System.out.println("Im in customDestroyMethod for user Bean");
    }
}
