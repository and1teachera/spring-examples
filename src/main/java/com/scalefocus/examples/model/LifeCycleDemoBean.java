package com.scalefocus.examples.model;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Created by jt on 6/5/17.
 */
//@Component
public class LifeCycleDemoBean implements InitializingBean
        , DisposableBean, // 22. define an initialization or destruction method for a Spring bean
        BeanNameAware, BeanFactoryAware, ApplicationContextAware {

    public LifeCycleDemoBean() {
        System.out.println("1. ## I'm in the LifeCycleBean Constructor");
    }

    @Override
    public void setBeanName(String name) {
        System.out.println(String.format(
                "2. ## My Bean Name is: %s and I'm called from setBeanName() method coming from BeanNameAware interface",
                name));
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        ((BeanDefinitionRegistry) beanFactory).registerBeanDefinition("myBean1",
                                                                      BeanDefinitionBuilder.genericBeanDefinition(
                                                                              String.class)
                                                                              .addConstructorArgValue("This is myBean1 added from setBeanFactory")
                                                                              .getBeanDefinition());
        final BeanDefinition lifeCycleDemoBean = ((BeanDefinitionRegistry) beanFactory).getBeanDefinition(
                "lifeCycleDemoBean");
        lifeCycleDemoBean.setInitMethodName("init1");

        System.out.println(
                "3. ## Bean Factory has been set! I'm called from method setBeanFactory() coming from BeanFactoryAware interface");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println(
                "4. ## Application context has been set! I'm called from setApplicationContext() method called from ApplicationContextAware interface");
    }

    public void beforeInit() {
        System.out.println("5. ## - Before Init - Called by Bean Post Processor");
    } // 18. BeanPostProcessor

    @PostConstruct  // 21. PostConstruct
    public void postConstruct() {
        System.out.println("6. ## The Post Construct annotated method has been called");
    }

    @Override // 22. initialization method
    public void afterPropertiesSet() throws Exception {
        System.out.println("7. ## The LifeCycleBean has its properties set! I'm called from afterPropertiesSet() coming from InitializingBean interface");
    }

    public void init() { // 19. initialization method
        System.out.println("8. ## - Custom init method. This method has been defined in the BeanFactoryPostProcessor and implemented in the bean!");
    }

    public void init1() { // 19. initialization method
        System.out.println("8.1. ## - Custom init method. This method has been defined in the BeanFactoryPostProcessor and implemented in the bean!");
    }

    public void afterInit() { // 18. BeanPostProcessor
        System.out.println("9. ## - After init called by Bean Post Processor");
    }

    @PreDestroy // 21. PreDestroy
    public void preDestroy() {
        System.out.println("1. ## The Predestroy annotated method has been called");
    }

    @Override // 22. destruction method
    public void destroy() throws Exception {
        System.out.println("2. ## The Lifecycle bean has been terminated");
    }

    public void customDestroy() { // 20. Destroy method
        System.out.println("3. ## - Custom destroy method");
    }

}
