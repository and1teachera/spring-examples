package com.scalefocus.examples.configuration;

import com.scalefocus.examples.model.LifeCycleDemoBean;
import com.scalefocus.examples.model.User;
import com.scalefocus.examples.processor.CustomFactoryPostProcessor;
import com.scalefocus.examples.processor.LifeCycleBeanPostProcessor;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author Angel Zlatenov
 */

@Configuration
public class BeanConfiguration {

    @Resource(name = "customFactoryPostProcessor")
    private CustomFactoryPostProcessor customFactoryPostProcessor;

    public BeanConfiguration(/*CustomFactoryPostProcessor customFactoryPostProcessor*/) { // 16. static Bean method
        //        this.customFactoryPostProcessor = customFactoryPostProcessor;
        System.out.println("Initializing bean configuration");
    }

    @Bean(initMethod = "init", destroyMethod = "customDestroy")  // 19. , 20. custom init method , custom destroy
    public LifeCycleDemoBean lifeCycleDemoBean() {
        return new LifeCycleDemoBean();
    }

    @Bean
    public User user() {
        //System.out.println("declaring user");
        return new User();
    }

    //    @Bean
    //    public static ExamplePostProcessor beanPostProcessor() {  // 16. static Bean method
    //        return new ExamplePostProcessor();
    //    }

    @Bean
    public static LifeCycleBeanPostProcessor lifeCycleBeanPostProcessor() {  // 16. static Bean method
        return new LifeCycleBeanPostProcessor();
    }

    @Bean
    public static BeanFactoryPostProcessor beanFactoryPostProcessor() {  // 16. static Bean method
        return factory -> {
            System.out.println("Im in beanFactoryPostProcessor");
            //            factory.addBeanPostProcessor(new ExamplePostProcessor());
            ((BeanDefinitionRegistry) factory).registerBeanDefinition("myBean",
                                                                      BeanDefinitionBuilder.genericBeanDefinition(
                                                                              String.class)
                                                                              .addConstructorArgValue("This is myBean")
                                                                              .getBeanDefinition());
            //
//            final BeanDefinition userBeanDefinition = factory.getBeanDefinition("user");
//            userBeanDefinition.setInitMethodName("customInitMethod");
//            userBeanDefinition.setDestroyMethodName("customDestroyMethod");
            //
//            final BeanDefinition lifeCycleBeanDefinition = factory.getBeanDefinition("lifeCycleDemoBean");
//            System.out.println("loading lifeCycleDemoBean Definition");
//            System.out.println("registering custom init and destroy methods");
//            System.out.println(lifeCycleBeanDefinition.getBeanClassName());
//            lifeCycleBeanDefinition.setInitMethodName("init");
//            lifeCycleBeanDefinition.setDestroyMethodName("customDestroy");
        };
    }

    @Bean
    public static CustomFactoryPostProcessor customFactoryPostProcessor() {
        return new CustomFactoryPostProcessor();
    }

    public void callToCustomFactoryPostProcessor() {
        System.out.println(customFactoryPostProcessor.hashCode());
    }

}
