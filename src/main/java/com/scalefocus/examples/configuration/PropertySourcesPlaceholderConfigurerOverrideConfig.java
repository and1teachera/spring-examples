package com.scalefocus.examples.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class PropertySourcesPlaceholderConfigurerOverrideConfig {

    @Bean // 17. PropertySourcesPlaceholderConfigurer
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        configurer.setPlaceholderPrefix("$$${");
        configurer.setPlaceholderSuffix("}}");
        configurer.setValueSeparator("|");

        return configurer;
    }
//
//    @Bean
//    public YamlPropertiesFactoryBean webPropertiesFromYamlFile() {
//        final YamlPropertiesFactoryBean yaml = new YamlPropertiesFactoryBean();
//        yaml.setResources(new ClassPathResource("application.yml"));
//        return yaml;
//    }
}
