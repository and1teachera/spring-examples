package com.scalefocus.examples.processor;

import com.scalefocus.examples.model.LifeCycleDemoBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;

public class LifeCycleBeanPostProcessor implements BeanPostProcessor, Ordered {

    public LifeCycleBeanPostProcessor() {
        System.out.println("Initializing lifecycle bean post processor");
    }

    @Override // 18. BeanPostProcessor
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof LifeCycleDemoBean) {
            System.out.println("postProcessBeforeInitialization in LifeCycleBeanPostProcessor");
            ((LifeCycleDemoBean) bean).beforeInit();
        }

        return bean;
    }

    @Override // 18. BeanPostProcessor
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof LifeCycleDemoBean) {
            System.out.println("postProcessAfterInitialization in LifeCycleBeanPostProcessor");
            ((LifeCycleDemoBean) bean).afterInit();
        }
        //System.out.println("Bean '" + beanName + "' created : " + bean.toString());
        return bean;
    }

    @Override
    public int getOrder() {
        return 1;
    }
}
