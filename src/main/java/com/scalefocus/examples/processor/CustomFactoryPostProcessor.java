package com.scalefocus.examples.processor;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * @author Angel Zlatenov
 */

public class CustomFactoryPostProcessor implements BeanFactoryPostProcessor {

    public CustomFactoryPostProcessor() {
        System.out.println("CustomFactoryPostProcessor");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        //System.out.println("postProcessBeanFactory from CustomFactoryPostProcessor");
    }

}
