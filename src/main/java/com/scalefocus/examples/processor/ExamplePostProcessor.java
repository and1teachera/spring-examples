package com.scalefocus.examples.processor;

import com.scalefocus.examples.model.LifeCycleDemoBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

/**
 * @author Angel Zlatenov
 */

@Component
public class ExamplePostProcessor implements BeanPostProcessor, Ordered {

    public ExamplePostProcessor() {
        //System.out.println("ExamplePostProcessor");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof LifeCycleDemoBean) {
            //System.out.println("postProcessBeforeInitialization in ExamplePostProcessor");
        }
        //System.out.println("Bean '" + beanName + "' created : " + bean.toString());
        //System.out.println("postProcessBeforeInitialization");
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {

        if (bean instanceof LifeCycleDemoBean) {
            //System.out.println("postProcessAfterInitialization in ExamplePostProcessor");
        }
        //System.out.println("Bean '" + beanName + "' created : " + bean.toString());
        return bean;
    }

    @Override
    public int getOrder() {
        return 2;
    }
}
