package com.scalefocus.examples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author Angel Zlatenov
 */

//@ComponentScan(basePackages = {
//        "com.scalefocus.demopackageA",
//        "com.scalefocus.demopackageD",
//        "com.scalefocus.demopackageE"
//    },
//    basePackageClasses = ExampleRepository.class)
//@ComponentScan(basePackages = "com.scalefocus", /*basePackageClasses = ExampleRepository.class,*/
//    includeFilters = {
//                @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*[Repository]") }, useDefaultFilters = false
//)
@SpringBootApplication
//@ComponentScan(basePackages = "com.scalefocus"/*, excludeFilters = {
//        @ComponentScan.Filter(type = FilterType.ANNOTATION, value = MyBean.class) }*/
//                /*@ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*[Repository]"*/ /*, useDefaultFilters = false*/
//)
public class SpringExamplesApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(SpringExamplesApplication.class,
                                                                                        args);

        //System.out.println(applicationContext.getBean("myBean"));
        //
        //        // 17. PropertySourcesPlaceholderConfigurer
        //        final User user = applicationContext.getBean("user", User.class);
        //        System.out.println(user.getName());
        //        System.out.println(user.getEmail());
//        //
//        final ExampleRepository exampleRepository = applicationContext.getBean("exampleRepository",
//                                                                               ExampleRepository.class);
//        System.out.println(exampleRepository.fetch());
//        //
        //
        //
        //                final BeanConfiguration beanConfiguration = applicationContext.getBean("beanConfiguration",
        //                                                                                       BeanConfiguration.class);
        //                beanConfiguration.callToCustomFactoryPostProcessor();
        //applicationContext.addBeanFactoryPostProcessor(new CustomFactoryPostProcessor());

//        System.out.println(applicationContext.getBean("myBean1"));

    }
}
