package com.scalefocus.examples.service;

/**
 * @author Angel Zlatenov
 */

public class UserService {

    public String sayHello(String name) {
        return "Hello " + name;
    }

    public Integer lengthOfName(String name) {
        return name.length();
    }

    public String sayBye(String name) {
        return name;
    }
}
