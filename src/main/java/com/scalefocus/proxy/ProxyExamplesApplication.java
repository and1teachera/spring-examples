package com.scalefocus.proxy;

import com.scalefocus.examples.service.UserService;
import com.scalefocus.proxy.handler.TimingDynamicInvocationHandler;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cglib.proxy.Proxy;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Angel Zlatenov
 */

@SpringBootApplication
public class ProxyExamplesApplication {

    public static void main(String[] args) {
        final ConfigurableApplicationContext applicationContext = SpringApplication.run(ProxyExamplesApplication.class,
                                                                                        args);

        UserService userService = new UserService();
        System.out.println(userService.sayHello("James"));

//        Enhancer enhancer = new Enhancer();
//        enhancer.setSuperclass(UserService.class);
////        enhancer.setCallback((FixedValue) () -> "Hello Tom!");
////        UserService fixedValueProxy = (UserService) enhancer.create();
////        System.out.println(fixedValueProxy.sayHello(null));
////        System.out.println(fixedValueProxy.lengthOfName("Tom"));
//
//        enhancer.setCallback((MethodInterceptor) (obj, method, arguments, proxy) -> {
//            if (method.getDeclaringClass() != Object.class && method.getName().equals("sayHello")) {
//                return "Hello " + arguments[0] + " from MethodInterceptor";
//            }
//            else {
//                return proxy.invokeSuper(obj, arguments);
//            }
//        });
//
//        UserService proxy = (UserService) enhancer.create();
//        System.out.println(proxy.sayHello("Tom"));
//        System.out.println(proxy.sayBye("Tom"));
//        System.out.println(proxy.lengthOfName("Tom"));
//
        Map<String, String> mapProxyInstance = (Map<String, String>) Proxy.newProxyInstance(
                ProxyExamplesApplication.class.getClassLoader(),
                new Class[] { Map.class },
                new TimingDynamicInvocationHandler(new HashMap<>()));

        mapProxyInstance.put("hello", "world");

        CharSequence csProxyInstance = (CharSequence) Proxy.newProxyInstance(
                ProxyExamplesApplication.class.getClassLoader(), new Class[] { CharSequence.class },
                new TimingDynamicInvocationHandler("Hello World"));

        csProxyInstance.length();
    }

}
