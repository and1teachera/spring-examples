package com.scalefocus.component;

import org.springframework.stereotype.Repository;

/**
 * @author Angel Zlatenov
 */
@MyBean
@Repository
public class ExampleRepository {

    public String fetch(){
        return "fetch method from ExampleRepository";
    }
}
